import { Request, Response } from 'express';
import { AppDataSource } from "../data-source";
import { CatPais } from '../entities/catPais.entity';

const catPaisRepository = AppDataSource.getRepository(CatPais);
export const getcatPaisRepositoryList = async (req: Request, res: Response) => {
  try {
    const user = await catPaisRepository.find()
    res.status(200).json(user);
  } catch (error) {
    console.error('Error al obtener la lista de CatOrigen:', error);
    res.status(500).json({ error: 'Error al obtener la lista de CatOrigen' });
  }
};
