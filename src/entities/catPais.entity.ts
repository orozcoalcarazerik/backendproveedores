import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class CatPais {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    descripcion!: string;
}
